#ifndef __HELPERS_H__
#define __HELPERS_H__

#include <Adafruit_NeoPixel.h>
#include <ESP8266TrueRandom.h>

inline uint32_t randColor() {
    uint32_t colors[] = {
        Adafruit_NeoPixel::Color(255, 0, 0), // red
        Adafruit_NeoPixel::Color(0, 255, 0), // green
        Adafruit_NeoPixel::Color(0, 0, 255), // blue
        Adafruit_NeoPixel::Color(255, 240, 40), // yellow
        Adafruit_NeoPixel::Color(190, 40, 255), // violet
        Adafruit_NeoPixel::Color(255, 170, 40), // orange
        Adafruit_NeoPixel::Color(0, 255, 255), // ligh blue
        Adafruit_NeoPixel::Color(255, 255, 255) // white
    };

    return colors[ESP8266TrueRandom.random(8)];
}

#endif