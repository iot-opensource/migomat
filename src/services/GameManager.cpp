#include "GameManager.h"

GameManager::GameManager(Device *irLed) {
    this->irLed = irLed;

    irLed->disable();
};
GameManager::~GameManager() {
    delete this->hosts;
    delete this->guests;
};

bool GameManager::start() {
    if (
        this->gameStatus != MatchStatus::WAITING_FOR_TEAMS ||
        this->hosts == NULL ||
        this->guests == NULL
    ) {
        return false;
    }

    if (!this->hosts->presence || !this->guests->presence) {
        return false;
    }

    this->gameStatus = MatchStatus::IN_PROGRESS;
    this->hosts->points->runSpark();
    this->guests->points->runSpark();
    this->irLed->enable();

    Log.traceln("Game started");

    return true;
}

bool GameManager::end() {
    Log.traceln("End game");

    if (this->gameStatus != MatchStatus::IN_PROGRESS) {
        return false;
    }

    this->gameStatus = MatchStatus::FINISHED;
    this->irLed->disable();

    return true;
}

void GameManager::setGuests(Team* team) {
    Log.traceln("Set guests");

    delete this->guests;
    this->guests = team;
    this->gameStatus = MatchStatus::WAITING_FOR_TEAMS;
}

void GameManager::setHosts(Team* team) {
    Log.traceln("Set hosts");

    delete this->hosts;
    this->hosts = team;
    this->gameStatus = MatchStatus::WAITING_FOR_TEAMS;
}

Team* GameManager::getGuests() {
    return this->guests;
}

Team* GameManager::getHosts() {
    return this->hosts;
}

MatchStatus GameManager::getMatchStatus() {
    return this->gameStatus;
}

void GameManager::addGoalForHosts() {
    Log.traceln("Hosts score a point");

    this->hosts->score += 1;

    this->lastGoalTime = millis() / 1000;
    this->hosts->points->displayScore(this->hosts->score);
}

void GameManager::addGoalForGuests() {
    Log.traceln("Guests score a point");

    this->guests->score += 1;

    this->lastGoalTime = millis() / 1000;
    this->guests->points->displayScore(this->guests->score);
}

void GameManager::undoGoalForHosts() {
    Log.traceln("Undo goal for hosts");

    if (this->hosts->score > 0) {
        this->hosts->score -= 1;
    }

    this->hosts->points->displayScore(this->hosts->score);
}

void GameManager::undoGoalForGuests() {
    Log.traceln("Undo goal for guests");

    if (this->guests->score > 0) {
        this->guests->score -= 1;
    }

    this->guests->points->displayScore(this->guests->score);
}

bool GameManager::isRanked() {
    return this->ranked;
}

void GameManager::markAsRanked() {
    this->ranked = true;
}

void GameManager::markAsOffline() {
    this->ranked = false;
}

Team* GameManager::getWinner() {
    if (this->hosts->score >= 10 && this->hosts->score - this->guests->score >= 2) {
        return this->hosts;
    }

    if (this->guests->score >= 10 && this->guests->score - this->hosts->score >= 2) {
        return this->guests;
    }

    return NULL;
}

int GameManager::getLastGoalTime() {
    return this->lastGoalTime;
}