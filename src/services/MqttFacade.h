#ifndef __MQTT_LISTENER_H__
#define __MQTT_LISTENER_H__

#include <Arduino.h>
#include <ArduinoLog.h>
#include <ArduinoJson.h>
#include <AsyncMqttClient.h>
#include "GameManager.h"
#include "../models/Points.h"

#define QoS_2 2

class MqttFacade {
    GameManager* gameManager;
    AsyncMqttClient* mqttClient;

    void publish(const char* topic, DynamicJsonDocument message);
public:
    MqttFacade(
        GameManager *gameManager, 
        AsyncMqttClient* client
    );
    ~MqttFacade();
    void initGame(
        DynamicJsonDocument payload,
        Points* hostsPoints,
        Points* guestsPoints
    );
    void quitGame(DynamicJsonDocument payload);
    void emitResult();
    void emitError(String message);
    void emitWin(Team* team);
};

#endif