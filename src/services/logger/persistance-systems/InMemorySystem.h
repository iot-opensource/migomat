#ifndef __IN_MEMORY_SYSTEM_H__
#define __IN_MEMORY_SYSTEM_H__

#include "../PersistanceSystem.h"

class InMemorySystem : public PersistanceSystem
{
    unsigned int limit;
    vector<String> logs;

public:
    InMemorySystem(unsigned int limit = 100);
    ~InMemorySystem();
    void add(String log);
    vector<String> readAll();
};

#endif