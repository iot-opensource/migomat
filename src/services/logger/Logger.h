#ifndef __LOGGER_LIBRARY_H__
#define __LOGGER_LIBRARY_H__

#include <Arduino.h>
#include <ESPAsyncWebServer.h>
#include <ESPDateTime.h>
#include <ArduinoJson.h>
#include "PersistanceSystem.h"

enum class LogLevel {
    SILENT = 0,
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL
};

class LoggerLibrary
{
    LogLevel limitLevel = LogLevel::TRACE;
    HardwareSerial *serial = NULL;
    DateTimeClass *datetime = NULL;
    PersistanceSystem *persistanceSystem = NULL;

    template <class T, typename... Args> 
    void add(LogLevel level, T msg, Args... args) {
        if (level < this->limitLevel) { return; }

        size_t needed = snprintf(NULL, 0, msg, args...);
        char  *buffer = (char*)malloc(needed+1);
        sprintf(buffer, msg, args...);
        
        String log = this->formatLogMessage(buffer, level);

        if (this->serial != NULL) {
            this->serial->println(log);
        }

        if (this->persistanceSystem != NULL) {
            this->persistanceSystem->add(log);
        }

        free(buffer);
    };
    String logLevelToString(LogLevel level);
    String formatLogMessage(const char *message, LogLevel level);

public:
    LoggerLibrary();
    ~LoggerLibrary();

    void begin(LogLevel level, PersistanceSystem *persistanceSystem);
    void useSerial(HardwareSerial *serial);
    void useEndpoints(AsyncWebServer *httpServer);
    void useDatetime(DateTimeClass *datetime);

    template <class T, typename... Args> 
    void trace(T msg, Args... args) {
        this->add(LogLevel::TRACE, msg, args...);
    }
    template <class T, typename... Args> 
    void debug(T msg, Args... args) {
        this->add(LogLevel::DEBUG, msg, args...);
    }
    template <class T, typename... Args> 
    void info(T msg, Args... args) {
        this->add(LogLevel::INFO, msg, args...);
    }
    template <class T, typename... Args> 
    void warn(T msg, Args... args) {
        this->add(LogLevel::WARN, msg, args...);
    }
    template <class T, typename... Args> 
    void error(T msg, Args... args) {
        this->add(LogLevel::ERROR, msg, args...);
    }
    template <class T, typename... Args> 
    void fatal(T msg, Args... args) {
        this->add(LogLevel::FATAL, msg, args...);
    }
};

static LoggerLibrary Logger;
extern LoggerLibrary Logger;

#endif