#include "Logger.h"

LoggerLibrary::LoggerLibrary() {};
LoggerLibrary::~LoggerLibrary() {};

void LoggerLibrary::begin(LogLevel level, PersistanceSystem *persistanceSystem) {
    this->limitLevel = level;
    this->persistanceSystem = persistanceSystem;
}

void LoggerLibrary::useSerial(HardwareSerial *serial) {
    this->serial = serial;
}

void LoggerLibrary::useDatetime(DateTimeClass *datetime) {
    this->datetime = datetime;
}

void LoggerLibrary::useEndpoints(AsyncWebServer *httpServer) {
    if (this->persistanceSystem == NULL) { return; }

    httpServer->on("/api/v1/logs", HTTP_GET, [&](AsyncWebServerRequest *request) {
        vector<String> logs = this->persistanceSystem->readAll();

        DynamicJsonDocument doc(4000);
        JsonArray array = doc.to<JsonArray>();

        for (unsigned int i = 0; i < logs.size(); i++) {
            array.add(logs[i]);
        }

        String json;
        serializeJson(doc, json);

        request->send(200, "application/json", json);
    });
}

String LoggerLibrary::logLevelToString(LogLevel level) {
    String levelsMapping[7] = { "SILENT", "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" };

    return levelsMapping[static_cast<int>(level)];
}

String LoggerLibrary::formatLogMessage(const char *message, LogLevel level) {
    String result;
    
    // decorate datetime
    if (this->datetime != NULL) {
        result += "[" + DateTime.format(DateFormatter::SIMPLE) + "] ";
    }
    // decorate loglevel
    result += this->logLevelToString(level) + ": ";

    result += String(message);

    return result;
}