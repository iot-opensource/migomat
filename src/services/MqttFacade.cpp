#include "MqttFacade.h"

MqttFacade::MqttFacade(GameManager* manager, AsyncMqttClient* client) {
    this->gameManager = manager;
    this->mqttClient = client;    
}

MqttFacade::~MqttFacade() {}

void MqttFacade::initGame(
    DynamicJsonDocument payload,
    Points* hostsPoints,
    Points* guestsPoints
) {
    if (
        !payload.containsKey("hosts") 
        || !payload.containsKey("guests") 
    ) {
        this->emitError("Game init error: need hosts and guests info");
        return;
    }
    
    // todo: walidacja na te pola inne

    hostsPoints->setColor(
        payload["hosts"]["color"].as<String>()
    );
    guestsPoints->setColor(
        payload["guests"]["color"].as<String>()
    );

    this->gameManager->setHosts(new Team(
        hostsPoints,
        payload["hosts"]["name"],
        payload["hosts"]["player1"],
        payload["hosts"]["player2"]
    ));
    this->gameManager->setGuests(new Team(
        guestsPoints,
        payload["guests"]["name"],
        payload["guests"]["player1"],
        payload["guests"]["player2"]
    ));
}

void MqttFacade::quitGame(DynamicJsonDocument payload) {
    Log.noticeln("Quit game by MQTT");
}

void MqttFacade::emitResult() {
    Log.noticeln(
        "Emit game result. Guests: %d vs Hosts: %d", 
        this->gameManager->getGuests()->score, 
        this->gameManager->getHosts()->score
    );

    DynamicJsonDocument json(1024);
    json["guests"]["points"] = this->gameManager->getGuests()->score;
    json["hosts"]["points"] = this->gameManager->getHosts()->score;
    json["datetime"] = 0;

    this->publish("game/info", json);
}

void MqttFacade::emitWin(Team* team) {
    Log.noticeln(
        "Emit winner. The winner is %s team.", 
        team->name
    );

    DynamicJsonDocument json(1024);
    json["winner"]["points"] = team->score;
    json["winner"]["color"] = team->points->getColorAsString();
    json["winner"]["name"] = team->name;
    json["datetime"] = 0;

    this->publish("game/end", json);
}

void MqttFacade::emitError(String message) {
    Log.errorln(
        "Emit error. %s", 
        message
    );

    DynamicJsonDocument json(1024);
    json["error"]["message"] = this->gameManager->getGuests()->score;

    this->publish("game/error", json); 
}

void MqttFacade::publish(const char* topic, DynamicJsonDocument message) {
    if (!gameManager->isRanked()) return;

    String parsedJson;
    serializeJson(message, parsedJson);
    
    Log.verboseln("Emited message: %s", parsedJson);

    this->mqttClient->publish(topic, QoS_2, true, parsedJson.c_str());
}