#ifndef __GAME_MANAGER_H__
#define __GAME_MANAGER_H__

#include <Arduino.h>
#include <ArduinoLog.h>
#include "../../include/consts.h"
#include "../models/Points.h"
#include "../models/Device.h"
#include "../models/Team.h"

enum class MatchStatus {
    NONE = 0,
    WAITING_FOR_TEAMS,
    IN_PROGRESS,
    FINISHED
};


class GameManager {
    Team* guests = NULL;
    Team* hosts = NULL;
    MatchStatus gameStatus = MatchStatus::NONE;
    bool ranked = false;
    int lastGoalTime = 0;
    Device* irLed;

public:
    GameManager(Device* irLed);
    ~GameManager();
    bool start();
    bool end();
    void setGuests(Team* team);
    void setHosts(Team* team);
    Team* getGuests();
    Team* getHosts();
    MatchStatus getMatchStatus();
    void addGoalForHosts();
    void addGoalForGuests();
    void undoGoalForHosts();
    void undoGoalForGuests();
    bool isRanked();
    void markAsRanked();
    void markAsOffline();
    Team* getWinner();
    int getLastGoalTime();
};

#endif