#include "Device.h"

Device::Device(uint8_t pin, bool invert = false)
{
    this->pin = pin;
    this->invert = invert;

    pinMode(pin, OUTPUT);
}

Device::~Device()
{
}

void Device::enable() {
    this->run = true;
    digitalWrite(this->pin, this->invert ? LOW : HIGH);
}

void Device::disable() {
    this->run = false;
    digitalWrite(this->pin, this->invert ? HIGH : LOW);
}

bool Device::isEnabled() {
    return this->run;
}

