#ifndef __DEVICE_H__
#define __DEVICE_H__

#include <Arduino.h>

class Device
{
	uint8_t pin;
    bool run;
    bool invert = false;

public:
	Device(uint8_t pin, bool invert);
	~Device();
    void enable();
    void disable();
    bool isEnabled();
};

#endif //__DEVICE_H__
