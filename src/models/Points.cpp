#include "Points.h"

Points::Points(uint8_t pin, Ticker* timer, uint32_t color) {
    this->strip = new Adafruit_NeoPixel(POINTS_LENGTH, pin, NEO_GRB + NEO_KHZ800);
    this->timer = timer;
    this->color = color != 0 ? color : randColor();
      
    this->strip->setBrightness(50);
    this->strip->begin();
    this->strip->clear();
}

Points::~Points() {
    delete this->strip;
};

void Points::setColor(uint32_t color) {
    this->color = color;
}

String Points::getColorAsString() {
    return String(color);
}

void Points::setColor(String color) {
    // if (color == "red") {
    //     this->color = CRGB::Red; 
    // }
}

void Points::runSpark() {
    this->animationRunning = true;
    this->timer->start();
}

void Points::showSpark() {
    this->strip->clear();

    int SPARK_SIZE = 5;
    static int sparkPos = 0 - SPARK_SIZE;

    for (int i = 0, j = 0; i < POINTS_LENGTH; i++) {
        if (i - SPARK_SIZE > sparkPos || i < sparkPos) {
            this->strip->setPixelColor(i, 0);
        } else {
            this->strip->setPixelColor(i, this->strip->ColorHSV(160, 255, sparkBrightness[j++]));
        }
    }

    this->strip->show();

    sparkPos += 1;

    if (sparkPos > POINTS_LENGTH) {
       sparkPos = 0 - SPARK_SIZE;
    }
}

void Points::displayScore(int score) {
    this->strip->clear();

    for (int i = 0; i < score % (POINTS_LENGTH + 1); i++) {
        this->strip->setPixelColor(i, this->color);
    }

    this->strip->show();
}

bool Points::isAnimationRunning() {
    return this->animationRunning;
}

void Points::offAnimation() {
    this->animationRunning = false;
    this->strip->clear();
    this->strip->show();
}