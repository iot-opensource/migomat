#ifndef __POINTS_H__
#define __POINTS_H__

#include <Arduino.h>
#include "../../include/consts.h"
#include "../../include/helpers.h"
#include <Adafruit_NeoPixel.h>
#include <Ticker.h>

class Points {
    uint32_t color;
    int sparkBrightness[5] = {
        80, 120, 180, 220, 255
    };
    Ticker* timer;
    bool animationRunning = false;
    Adafruit_NeoPixel* strip;

public:
    Points(uint8_t pin, Ticker* timer, uint32_t color = 0);
    ~Points();
    void setColor(uint32_t color);
    void setColor(String color);
    String getColorAsString();
    void showSpark();
    void runSpark();
    void displayScore(int score);
    bool isAnimationRunning();
    void offAnimation();
};

#endif