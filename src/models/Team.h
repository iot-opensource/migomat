#ifndef _TEAM_H__
#define _TEAM_H__

#include <Arduino.h>
#include "../../include/consts.h"
#include "Points.h"

class Team {
public:
    int score;
    Points* points;
    const char* player1;
    const char* player2;
    const char* name;
    bool presence;

    Team(Points* points, const char *name);
    Team(
        Points* points, 
        const char *name,
        const char *player1, 
        const char *player2
    );
    ~Team();
};

#endif