#include "Team.h"

Team::Team(Points* points, const char *name) {
    this->score = 0;
    this->points = points;
    this->presence = true;
    this->name = name;
    this->player1 = "Alvaro Morata";
    this->player2 = "Zbigniew Boniek";
}

Team::Team(
    Points* points, 
    const char *name,
    const char *player1, 
    const char *player2
) {
    this->score = 0;
    this->points = points;
    this->presence = true;
    this->player1 = player1;
    this->player2 = player2;
}

Team::~Team() {}