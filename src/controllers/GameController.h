#ifndef __GAME_CONTROLLER_H__
#define __GAME_CONTROLLER_H__

#include <Arduino.h>
#include <AsyncJson.h>
#include <ESPAsyncWebServer.h>
#include "../services/GameManager.h"

class GameController {
    GameManager *gameManager;
public:
    GameController(GameManager *gameManager);
    ~GameController();
    void getRoute(AsyncWebServerRequest *request);
};

#endif