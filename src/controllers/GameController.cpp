#include "GameController.h"

GameController::GameController(GameManager *gameManager) {
    this->gameManager = gameManager;
}

GameController::~GameController() {}

void GameController::getRoute(AsyncWebServerRequest *request) {
    DynamicJsonDocument json(1024);
    if (this->gameManager->getGuests() != NULL) {
        json["guests"]["points"] = this->gameManager->getGuests()->score;
        json["guests"]["presence"] = this->gameManager->getGuests()->presence;
        json["guests"]["color"] = this->gameManager->getGuests()->points->getColorAsString();
        json["guests"]["player1"] = this->gameManager->getGuests()->player1;
        json["guests"]["player2"] = this->gameManager->getGuests()->player2;
    }
    
    if (this->gameManager->getHosts() != NULL) {
        json["hosts"]["points"] = this->gameManager->getHosts()->score;
        json["hosts"]["presence"] = this->gameManager->getHosts()->presence;
        json["hosts"]["color"] = this->gameManager->getHosts()->points->getColorAsString();
        json["hosts"]["player1"] = this->gameManager->getHosts()->player1;
        json["hosts"]["player2"] = this->gameManager->getHosts()->player2;
    }

    String parsedJson;
    serializeJson(json, parsedJson);
    
    request->send(200, "application/json", parsedJson);
}