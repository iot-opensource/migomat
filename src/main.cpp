#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Hash.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <AsyncMqttClient.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <ESPDateTime.h>
#include <TZ.h>
#include <ArduinoLog.h>
#include "services/GameManager.h"
#include "services/MqttFacade.h"
#include "services/logger/Logger.h"
#include "services/logger/persistance-systems/InMemorySystem.h"
#include "models/Points.h"
#include "models/Team.h"
#include "models/Device.h"
#include "controllers/GameController.h"
#include "../include/consts.h"

AsyncWebServer* httpServer;
AsyncMqttClient* mqttClient;
MqttFacade* mqttFacade;
GameManager* gameManager;
Points* hostsPoints;
Points* guestsPoints;
GameController* gameController;
Ticker* hostsSparkAnimationTimer;
Ticker* guestsSparkAnimationTimer;
Device* irLed;

void runHostsSparkAnimation() {
    hostsPoints->showSpark();
}

void runGuestsSparkAnimation() {
    guestsPoints->showSpark();
}

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void onMqttConnect(bool sessionPresent) {
    Log.noticeln("Connected to MQTT.");

    mqttFacade = new MqttFacade(
        gameManager, 
        mqttClient
    );

    mqttClient->subscribe("game/init", QoS_2);
    mqttClient->subscribe("game/quit", QoS_2);
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
    Log.noticeln("Subscribe acknowledged.");
}

void onMqttUnsubscribe(uint16_t packetId) {
    Log.noticeln("Unsubscribe acknowledged.");
}

void setup() {
    Serial.begin(9600);
    Log.begin(LOG_LEVEL, &Serial);
    Log.setShowLevel(true);

    PersistanceSystem *sys = new InMemorySystem();
    Logger.begin(LogLevel::TRACE, sys);
    Logger.useSerial(&Serial);

    pinMode(BUTTON_GUESTS, INPUT);
    pinMode(BUTTON_HOSTS, INPUT);
    pinMode(GATE_GUESTS, INPUT);
    pinMode(GATE_HOSTS, INPUT);
    pinMode(LED_GUESTS, OUTPUT);
    pinMode(LED_HOSTS, OUTPUT);

    irLed = new Device(IR_LED_PIN, true);
    httpServer = new AsyncWebServer(HTTP_SERVER_PORT);
    mqttClient = new AsyncMqttClient(); 
    hostsSparkAnimationTimer = new Ticker(runHostsSparkAnimation, 100, 36);
    guestsSparkAnimationTimer = new Ticker(runGuestsSparkAnimation, 100, 36);
    hostsPoints = new Points(POINTS_PIN_HOSTS, hostsSparkAnimationTimer);
    guestsPoints = new Points(POINTS_PIN_GUESTS, guestsSparkAnimationTimer);
    gameManager = new GameManager(irLed);
    gameController = new GameController(gameManager);

    mqttClient->onConnect(onMqttConnect);
    mqttClient->onSubscribe(onMqttSubscribe);
    mqttClient->onUnsubscribe(onMqttUnsubscribe);
    mqttClient->onMessage([](char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
        DynamicJsonDocument doc(1024);
        DeserializationError error = deserializeJson(doc, payload);

        if (error) {
            Log.errorln("Deserialization payload error");
            return;
        }

        Log.verboseln("Topic: %s", topic);
        Log.verboseln("Payload: %s", payload);
        
        if (String(topic) == "game/init") {
            mqttFacade->initGame(doc, hostsPoints, guestsPoints);
        } else if (String(topic) == "game/quit") {
            mqttFacade->quitGame(doc);
        } else if (String(topic) == "game/info") {
            mqttFacade->emitResult();
        } else {
            Log.warningln("Wrong topic: %s", topic);
        }
    });

    WiFiManager.begin(httpServer);

    Logger.useEndpoints(httpServer);

    httpServer->on("/api/v1/game", HTTP_GET, [](AsyncWebServerRequest *request) {
        gameController->getRoute(request);
    });

    mqttClient->setServer(MQTT_HOST, MQTT_PORT);
    mqttClient->setCredentials(MQTT_LOGIN, MQTT_PASSWORD);
    mqttClient->connect();
    httpServer->onNotFound(notFound);
    AsyncElegantOTA.begin(httpServer, OTA_USERNAME, OTA_PASSWORD);
    httpServer->begin();

    DateTime.begin();
    Logger.useDatetime(&DateTime);
    Log.noticeln("System is ready");

    hostsPoints->runSpark();
    guestsPoints->runSpark();
}

void loop() {
    if (digitalRead(BUTTON_HOSTS) == 0) {
        delay(20);
        Log.verbose("Pressed hosts button");
        int pushedTime = millis() / 1000;
        while (digitalRead(BUTTON_HOSTS) == 0) {ESP.wdtFeed();};

        if ((millis() / 1000) - pushedTime >= 5) { // 5 seconds
            Log.verboseln(" more then 5 sec");
        
            Team* winner = gameManager->getWinner();

            if (winner != NULL) {
                gameManager->end();
                mqttFacade->emitWin(winner);
                winner->points->runSpark();
            }
        } else if ((millis() / 1000) - pushedTime < 2) {
            Log.verboseln(" less then 2 sec");
            if (
                (
                    gameManager->getMatchStatus() == MatchStatus::NONE ||
                    gameManager->getMatchStatus() == MatchStatus::WAITING_FOR_TEAMS 
                )
                && gameManager->getHosts() == NULL
            ) {
                gameManager->setHosts(
                    new Team(hostsPoints, "Barcelona")
                );
                digitalWrite(LED_HOSTS, HIGH);
            } else if (
                gameManager->getMatchStatus() == MatchStatus::IN_PROGRESS
            ) {
                gameManager->undoGoalForHosts();
                mqttFacade->emitResult();
            }
        }
    }

    if (digitalRead(BUTTON_GUESTS) == 0) {
        delay(20);
        Log.verbose("Pressed guests button");
        int pushedTime = millis() / 1000;
        while (digitalRead(BUTTON_GUESTS) == 0) {ESP.wdtFeed();};

        if ((millis() / 1000) - pushedTime >= 5) { // 5 seconds
            Log.verboseln(" more then 5 sec");
        
            Team* winner = gameManager->getWinner();

            if (winner != NULL) {
                gameManager->end();
                mqttFacade->emitWin(winner);
                winner->points->runSpark();
            }
        } else if ((millis() / 1000) - pushedTime < 2) {
            Log.verboseln(" less then 2 sec");
            if (
                (
                    gameManager->getMatchStatus() == MatchStatus::NONE ||
                    gameManager->getMatchStatus() == MatchStatus::WAITING_FOR_TEAMS 
                )
                && gameManager->getGuests() == NULL
            ) {
                gameManager->setGuests(
                    new Team(guestsPoints, "Madryt")
                );
                digitalWrite(LED_GUESTS, HIGH);
            } else if (
                gameManager->getMatchStatus() == MatchStatus::IN_PROGRESS
            ) {
                gameManager->undoGoalForGuests();
                mqttFacade->emitResult();
            }
        }
    }

    if (gameManager->start()) {
        mqttFacade->emitResult();
    }

    if (digitalRead(GATE_HOSTS) == 1 && irLed->isEnabled() && (millis() / 1000) - gameManager->getLastGoalTime() > 2) {
        gameManager->addGoalForHosts();
        mqttFacade->emitResult();
    }

    if (digitalRead(GATE_GUESTS) == 1 && irLed->isEnabled() && (millis() / 1000) - gameManager->getLastGoalTime() > 2) {
        gameManager->addGoalForGuests();
        mqttFacade->emitResult();
    }

    if (hostsSparkAnimationTimer->state() == RUNNING) {
        hostsSparkAnimationTimer->update();
    } else {
        if (hostsPoints->isAnimationRunning()) {
            hostsPoints->offAnimation();
        }
    }

    if (guestsSparkAnimationTimer->state() == RUNNING) {
        guestsSparkAnimationTimer->update();
    } else {
        if (guestsPoints->isAnimationRunning()) {
            guestsPoints->offAnimation();
        }
    }
}